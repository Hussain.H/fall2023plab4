package linearalgebra;
// Haris Hussain
// 2234354

public class Vector3d {
    private double x;
    private double y;
    private double z;

Vector3d(double x, double y, double z){
this.x =x;
this.y=y;
this.z=z;
}

public double getX() {
    return x;
}
public double getY() {
    return y;
}
public double getZ() {
    return z;
}

public double magnitude(){
    return Math.sqrt((Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2)));
}
public double doProduct(Vector3d vector){
return (this.getX() * vector.getX()) + (this.getY()* vector.getY()) + (this.getZ() * vector.getZ());
}

public Vector3d add(Vector3d vector){
    double newX = this.getX() + vector.getX();
    double newY = this.getY() + vector.getY();
    double newZ= this.getZ() + vector.getZ();
    Vector3d newVec = new Vector3d(newX, newY, newZ);
    return newVec;
}
public static void main(String[] args) {
    Vector3d vec1 = new Vector3d(1,1,2);
    Vector3d vec2 = new Vector3d(2,3,4);
    System.out.println(vec1.doProduct(vec2));

}
}
